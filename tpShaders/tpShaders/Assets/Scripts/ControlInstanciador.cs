using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlInstanciador : MonoBehaviour
{
    public List<GameObject> plataformas;
    public static int i = 0;
    public GameObject plataforma;
    private GameObject plataformaInstanciada;
    private float posY = 10;

    void Update()
    {
        if (i < 99)
        {
            plataformaInstanciada = Instantiate(plataforma);
            plataformaInstanciada.transform.position = new Vector3(0, posY, 0);
            plataformas.Add(plataformaInstanciada);
            posY += 10;
            i++;
        }
    }
}

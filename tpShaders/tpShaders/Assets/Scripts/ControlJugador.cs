using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private bool enSuelo = true;
    private int saltoActual = 0;
    private int maxSaltos = 1;
    public int magnitudSalto = 10;
    private Rigidbody rb;
    public static Transform posPJ;
    [HideInInspector] public static int puntos = 0;
    [SerializeField] private float rapidezDesplazamiento = 0;
    [HideInInspector] public static bool perdiste = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        transform.position = new Vector3(0, 3.15f, 0);
    }

    void Update()
    {
        posPJ = transform;
        float movimientoCostados = Input.GetAxis("Horizontal") * Time.deltaTime;
        movimientoCostados *= rapidezDesplazamiento;
        transform.Translate(movimientoCostados, 0, 0);

        if (transform.position.x >= 8)
        {
            transform.position = new Vector3(8, posPJ.position.y, 0);
        }
        else if (transform.position.x <= -8)
        {
            transform.position = new Vector3(-8, posPJ.position.y, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space) && (enSuelo || maxSaltos > saltoActual)) //este if es para el doble salto
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            enSuelo = false;
            saltoActual++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            enSuelo = true;

            if (!collision.gameObject.transform.parent.GetComponent<ControlPlataforma>().tocado)
            {
                puntos++;
                collision.gameObject.transform.parent.GetComponent<ControlPlataforma>().tocado = true;
            }
        }

        if (collision.gameObject.CompareTag("Bad"))
        {
            gameObject.GetComponent<ControlJugador>().enabled = false;
            gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            perdiste = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tar"))
        {
            gameObject.GetComponent<ControlJugador>().enabled = false;
            perdiste = true;
        }
    }
}

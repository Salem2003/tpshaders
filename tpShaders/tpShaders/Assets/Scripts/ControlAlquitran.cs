using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAlquitran : MonoBehaviour
{
    [SerializeField] private float velocidad = 0.0f;

    void Start()
    {
        transform.position = new Vector3(0, -10f, 0);
    }

    void Update()
    {
        transform.position += new Vector3(0, velocidad, 0) * Time.deltaTime;
    }
}

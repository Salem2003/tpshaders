using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    void Update()
    {
        transform.position = new Vector3(0, ControlJugador.posPJ.position.y + 2, -10);
        transform.GetChild(0).transform.LookAt(ControlJugador.posPJ.position);
    }
}

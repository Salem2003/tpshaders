using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataforma : MonoBehaviour
{
    [SerializeField] private float velocidad = 0.0f;
    private bool derecha = false;
    private int flag = 0;
    public bool tocado = false;

    void Start()
    {
        flag = 0;
        derecha = false;
        tocado = false;
    }

    void Update()
    {
        if (flag == 0 && Random.Range(0, 2) == 1)
        {
            flag = 1;
            derecha = true;
        }
        else
        {
            flag = 1;
        }

        if (derecha)
        {
            transform.position += new Vector3(velocidad, 0, 0) * Time.deltaTime;

            if (transform.position.x >= 10)
            {
                derecha = false;
            }
        }
        else if (!derecha)
        {
            transform.position -= new Vector3(velocidad, 0, 0) * Time.deltaTime;

            if (transform.position.x <= -10)
            {
                derecha = true;
            }
        }
    }
}

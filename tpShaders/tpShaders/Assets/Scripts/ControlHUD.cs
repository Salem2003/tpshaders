using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ControlHUD : MonoBehaviour
{
    public TMP_Text txtPuntos;
    public TMP_Text txtPerdiste;

    void Start()
    {
        txtPuntos.text = "Puntos: 0";
        txtPerdiste.text = "";
    }

    void Update()
    {
        txtPuntos.text = $"Puntos: {ControlJugador.puntos}";

        if (ControlJugador.perdiste)
        {
            txtPuntos.text = "";
            txtPerdiste.text = $"Game Over \nPuntos : {ControlJugador.puntos}";
        }
    }
}

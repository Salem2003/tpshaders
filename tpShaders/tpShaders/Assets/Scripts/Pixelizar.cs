using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Pixelizar : ScriptableRendererFeature
{
    [System.Serializable]

    public class CustomPassSettings
    {
        public RenderPassEvent rpe = RenderPassEvent.BeforeRenderingPostProcessing;
        public int altoPantalla = 144;
    }

    [SerializeField] private CustomPassSettings settings;
    private PixelPass customPass;

    public override void Create()
    {
        customPass = new PixelPass(settings);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
#if UNITY_EDITOR
        if (renderingData.cameraData.isSceneViewCamera) return;
#endif
        renderer.EnqueuePass(customPass);
    }
}

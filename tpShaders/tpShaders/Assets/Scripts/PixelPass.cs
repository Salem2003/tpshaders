using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PixelPass : ScriptableRenderPass
{
    private Pixelizar.CustomPassSettings settings;
    private RenderTargetIdentifier colorBuffer, pixelBuffer;
    private int pixelBufferID = Shader.PropertyToID("_PixelBuffer");

    private Material mat;
    private int alturaPantallaPixel, anchoPantallaPixel;
    
    public PixelPass(Pixelizar.CustomPassSettings settings)
    {
        this.settings = settings;
        this.renderPassEvent = settings.rpe;

        if (mat == null) mat = CoreUtils.CreateEngineMaterial("Hidden/Pixelize");
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        CommandBuffer cmd = CommandBufferPool.Get();

        using (new ProfilingScope(cmd, new ProfilingSampler("Pixelize Pass")))
        {
            Blit(cmd, colorBuffer, pixelBuffer, mat);
            Blit(cmd, pixelBuffer, colorBuffer);
        }

        context.ExecuteCommandBuffer(cmd);
        CommandBufferPool.Release(cmd);
    }

    public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
    {
        colorBuffer = renderingData.cameraData.renderer.cameraColorTarget;
        RenderTextureDescriptor descriptor = renderingData.cameraData.cameraTargetDescriptor;

        alturaPantallaPixel = settings.altoPantalla;
        anchoPantallaPixel = (int)(alturaPantallaPixel * renderingData.cameraData.camera.aspect + 0.5f);

        mat.SetVector("_BlockCount", new Vector2(anchoPantallaPixel, alturaPantallaPixel));
        mat.SetVector("_BlockSize", new Vector2(1.0f / anchoPantallaPixel, 1.0f / alturaPantallaPixel));
        mat.SetVector("_HalfBlockSize", new Vector2(0.5f / anchoPantallaPixel, 0.5f / alturaPantallaPixel));

        descriptor.height = alturaPantallaPixel;
        descriptor.width = anchoPantallaPixel;

        cmd.GetTemporaryRT(pixelBufferID, descriptor, FilterMode.Point);
        pixelBuffer = new RenderTargetIdentifier(pixelBufferID);
    }

    public override void OnCameraCleanup(CommandBuffer cmd)
    {
        if (cmd == null) throw new System.ArgumentNullException("cmd");
        cmd.ReleaseTemporaryRT(pixelBufferID);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirPJ : MonoBehaviour
{
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;

    void Update()
    {
        transform.position = new Vector3(x, ControlJugador.posPJ.position.y + y, -z);
    }
}
